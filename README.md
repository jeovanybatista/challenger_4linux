# Challenger_4Linux



## repositório utilizado para provisionamento de aplicações

Neste repositório você encontrará todos os arquivos de provisionamento para as aplicações Zabbix, Grafana e Prometheus.

## Arquivo de configuração

No arquivo prometheus.yml está o conteúdo do arquivo de configuração dos targets e jobs para leitura nos nodes.


## Clonando repositório

Para realizar o clone deste repositório basta digitar:

```
git clone https://gitlab.com/jeovanybatista/challenger_4linux.git

```

